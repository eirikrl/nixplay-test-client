'use strict';

angular.module('myApp.photodetails', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/photos/:id', {
    templateUrl: 'components/photodetails/photoDetailsView.html',
    controller: 'PhotoDetailsController'
  });
}])


.controller('PhotoDetailsController', ['$scope', '$routeParams', 'DataService', function($scope, $routeParams, DataService) {

	DataService.getDetails($routeParams.id).then(function (data) {

		$scope.photo = data;
	});


}])