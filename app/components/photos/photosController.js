'use strict';

angular.module('myApp.photos', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/photos', {
    templateUrl: 'components/photos/photosView.html',
    controller: 'PhotosCtrl'
  });
}])

.controller('PhotosCtrl', ['$scope', 'DataService', function($scope, DataService) {


	$scope.photos = DataService.list;

	DataService.getList();


}])