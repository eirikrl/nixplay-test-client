'use strict';

angular.module('myApp.topbar', [])

.controller('TopbarController', ['$scope', 'DataService', function($scope, DataService) {


    $scope.uploadFile = function () {

        var file = $scope.myFile;

        if(!file) {
        	alert('Please choose a file');
        	return;
        }

        var uploadUrl = 'http://0.0.0.0:3000/photo/upload';

        DataService.uploadFile(file, uploadUrl).then(function (data) {

        	DataService.setList(data);

        }).catch(function (err) {
        	alert(err);
        });
    };


} ]);