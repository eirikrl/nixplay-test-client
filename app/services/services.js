angular.module('myApp.services', [])

.factory('DataService', function($http, $q) {


    var list = [];

    var factory = {

        list: list,
        getList: function() {

            return $http.get('http://0.0.0.0:3000/photos/list').then(function(response) {

                list.length = 0;

                angular.copy(response.data, list);

                return response.data;
            });

        },
        setList: function (data) {
            
            list.length = 0;

            angular.copy(data, list);
        },
        getDetails: function(id) {

            return $http.get('http://0.0.0.0:3000/photos/list/' + id + '/details').then(function(response) {

                return response.data;
            });
        },
        uploadFile: function(file, uploadUrl) {

            var formData = new FormData();

            formData.append('data', file);

            return $http.post(uploadUrl, formData, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).then(function(response) {
                return response.data;
            });
        }    

    };

    return factory;

});
